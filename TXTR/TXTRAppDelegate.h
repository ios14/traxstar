//
//  TXTRAppDelegate.h
//  TXTR
//
//  Created by chetna sisodia on 08/10/14.
//  Copyright (c) 2014 ___vvdn___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TXTRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
